const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    mode:'development',
    entry:{
        app:'./src/index.js',
    },

    output:{
        path: path.resolve(__dirname,'dist'),
        filename:'[name].js',
        publicPath: '/dist'
    },
    devtool:'eval',
    devServer: {
        overlay:true, 
        port: 9000,
    },
    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['env']
                  }
                }
              },
              {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                  extractCSS: true
                }
              },
          
              {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                  })
              },
              {
                test: /\.jade$/,
                loader:'jade-loader',
                options:{
                    pretty:true
                }
              },
              
              {
                test: /\.(png|jpg|gif|svg|mov|mp4)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'img/',
                        publicPath:'img/'
                      }
                  }
                ]
              }
            
        ]

    },
    plugins: [
        //new HtmlWebpackPlugin(),
        /*new HtmlWebpackExcludeAssetsPlugin(),
        new webpack.ProvidePlugin({
            //$: 'jquery',
            //jQuery: 'jquery',
            //'Waves': 'node-waves',
            //bootstrap: 'bootstrap',
           
           // masonry:'masonry-layout'
            //video: 'video'
        }),*/
        new ExtractTextPlugin('style.css')
        
    ]

}

